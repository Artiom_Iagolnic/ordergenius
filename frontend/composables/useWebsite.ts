import {useWebsiteStore} from "~/store/website.store";
import {useErrorStore} from "~/store/error.store";

interface Website {
    url: string;
    requires_login: boolean;
    username: string | null;
    password: string | null;
}

export default function useWebsite() {
    const websiteStore = useWebsiteStore();
    const errorStore = useErrorStore();
    const {get, post, del, put} = useApi();

    const fetchAllWebsites = async () => {
        const data = await get("/websites");
        websiteStore.setWebsites(data.websites);
    }

    const addWebsite = async (website: Website) => {
        if (!website) return
        errorStore.clear()
        await post("/websites", website);
        await fetchAllWebsites()
    }
    const updateWebsite = async (website: Website, id: number) => {
        await put(`/websites/${id}`, website);
        await fetchAllWebsites()
    }
    const deleteWebsite = async (id: number) => {
        await del(`/websites/${id}`);
        await fetchAllWebsites()
    }

    return {
        fetchAllWebsites,
        addWebsite,
        deleteWebsite,
        updateWebsite
    }
}
import {useIsLoadingStore} from "~/store/loading.store";
import {useErrorStore} from "~/store/error.store";

export default function useApi() {
    const apiBaseUrl = process.env.API_BASE_URL || "http://localhost:8000/api";
    const loadingStore = useIsLoadingStore();
    const errorStore = useErrorStore();

    const getToken = () => {
        const tokenMatch = document.cookie.match(new RegExp(`(^| )XSRF-TOKEN=([^;]+)`));
        return tokenMatch ? decodeURIComponent(tokenMatch[2]) : null;
    };

    const request = async (url: string, options: RequestInit = {}) => {
        errorStore.clear();
        const csrfToken = getToken();
        if (!csrfToken) {
            errorStore.set("CSRF token not found");
            return;
        }
        options.headers = {
            ...options.headers,
            'X-XSRF-TOKEN': csrfToken || '',
            'Content-Type': 'application/json',
            'Accept': 'application/json',
        };
        options.credentials = 'include';
        try {
            const response = await fetch(`${apiBaseUrl}${url}`, options);
            if (!response.ok) {
                const errorData = await response.json();
                if (errorData.errors) {
                    errorStore.set(errorData.errors);
                } else {
                    errorStore.set(errorData.message || 'An error occurred');
                }
                throw new Error(response.statusText);
            }
            return await response.json();
        } catch (error) {
            console.error('API request error:', error);
        }
    };

    return {
        get: (url: string) => request(url, {method: 'GET'}),
        post: (url: string, body: any) => request(url, {method: 'POST', body: JSON.stringify(body)}),
        put: (url: string, body: any) => request(url, {method: 'PUT', body: JSON.stringify(body)}),
        del: (url: string) => request(url, {method: 'DELETE'}),
    };
}

import {Home, LineChart, Package, Package2, Settings, ShoppingCart, Users2} from "lucide-vue-next";

export interface IMenuItem {
    name: ReturnType<typeof defineComponent>;
    url: string;
    icon: ReturnType<typeof defineComponent>;
    tooltip: string;
}

export const MENU_DATA: { main: IMenuItem[], settings: IMenuItem[] } = {
    main: [
        {
            name: 'autoparts1190',
            url: '/',
            icon: Package2,
            tooltip: 'autoparts1190'
        },
        {
            name: 'dashboard',
            url: '/',
            icon: Home,
            tooltip: 'dashboard'
        },
        {
            name: 'orders',
            url: '/orders',
            icon: ShoppingCart,
            tooltip: 'orders'
        },
        {
            name: 'products',
            url: '/products',
            icon: Package,
            tooltip: 'products'
        },
        {
            name: 'customers',
            url: '/customers',
            icon: Users2,
            tooltip: 'customers'
        },
        {
            name: 'analytics',
            url: '/analytics',
            icon: LineChart,
            tooltip: 'analytics'
        }
    ],
    settings: [
        {
            name: 'settings',
            url: '/settings',
            icon: Settings,
            tooltip: 'settings'
        }
    ]
};


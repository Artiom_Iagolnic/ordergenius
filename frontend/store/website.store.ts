import {defineStore} from 'pinia';

interface Website {
    id: number;
    url: string;
    requires_login: boolean;
    username: string | null;
    password: string | null;
}

export const useWebsiteStore = defineStore('website', {
    state: () => ({
        websites: [] as Website[],
    }),
    actions: {
        setWebsites(websites: Website[]) {
            this.websites = websites;
        },
        addWebsite(website: Website) {
            this.websites.push(website);
        },
    },
});

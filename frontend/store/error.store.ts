interface ErrorState {
    error: string | null;
    fieldErrors: Record<string, string[]>;
}

export const useErrorStore = defineStore("error", {
    state: (): ErrorState => ({
        error: null,
        fieldErrors: {},
    }),

    actions: {
        set(data: any) {
            if (typeof data === 'string') {
                this.$patch({error: data, fieldErrors: {}});
            } else {
                this.$patch({error: null, fieldErrors: data});
            }
        },
        clear() {
            this.$patch({error: null, fieldErrors: {}});
        }
    },
});

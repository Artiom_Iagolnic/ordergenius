import {languages} from "~/lang";

export default defineI18nConfig(() => ({
    legacy: false,
    locale: 'en',
    messages: {
        en: languages.en,
        de: languages.de,
    },
}))
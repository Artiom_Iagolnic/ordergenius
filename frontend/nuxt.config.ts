import type { NuxtPage } from "@nuxt/schema";

// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
    workspaceDir: "",
    $development: undefined, $env: undefined, $meta: undefined, $production: undefined, $test: undefined,
    devtools: {enabled: true},
    css: ["~/assets/css/main.css"],
    postcss: {
        plugins: {
            tailwindcss: {},
            autoprefixer: {},
        },
    },
    modules: [
        "@pinia/nuxt",
        "shadcn-nuxt",
        '@nuxtjs/tailwindcss',
        "@nuxtjs/i18n",
        "nuxt-auth-sanctum"
    ],
    pinia: {
        storesDirs: ["~/store/**"],
    },
    shadcn: {
        prefix: '',
        componentDir: './components/ui'
    },
    i18n: {
        vueI18n: './i18n.config.ts',
        locales: ['en', 'de'],
        defaultLocale: 'de',
    },
    sanctum: {
        baseUrl: 'http://localhost:8000',
    },
});
import { Request, Response } from 'express';
import { scrapeWebsites } from '../services/puppeteerService';

export const scrapeController = async (req: Request, res: Response) => {
    const { query, websites } = req.body;
    try {
        const results = await scrapeWebsites(query, websites);
        res.json(results);
    } catch (error) {
        console.error('Error during scraping:', error);
        res.status(500).json({ error: 'Error during scraping' });
    }
};
import { Router } from 'express';
import { scrapeController } from './controllers/scrapperController';

const router = Router();

router.post('/scrape', scrapeController);

export default router;

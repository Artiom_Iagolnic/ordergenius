import puppeteer from 'puppeteer-extra';
import {Page} from 'puppeteer';
import StealthPlugin from 'puppeteer-extra-plugin-stealth';

puppeteer.use(StealthPlugin());

interface Website {
    url: string;
    requires_login: boolean;
    username?: string | null;
    password?: string | null;
}

export const scrapeWebsites = async (query: string, websites: Website[], maxPages = 2, itemsPerPage = 15) => {
    const results: Array<{ title: string, price: number, link: string, availability: string }> = [];

    const browser = await puppeteer.launch({
        headless: true,
        args: [
            '--no-sandbox',
            '--disable-setuid-sandbox',
            '--disable-dev-shm-usage',
            '--disable-accelerated-2d-canvas',
            '--disable-gpu',
            '--window-size=1920x1080',
        ],
    });

    try {
        for (const website of websites) {
            const page = await browser.newPage();

            try {
                await setupPageInterception(page);

                // Navigate to the website
                await page.goto(website.url, {waitUntil: 'domcontentloaded'});

                // If login is required, perform login
                if (website.requires_login && website.username && website.password) {
                    await loginToWebsite(page, website);
                }

                await performSearchAndExtract(page, query, results, maxPages);

            } catch (error) {
                console.error(`Error scraping ${website.url}:`, error);
            } finally {
                await page.close();
            }
        }

        return results.sort((a, b) => a.price - b.price).slice(0, itemsPerPage);

    } catch (error) {
        console.error('Error during scraping:', error);
        throw new Error('Scraping failed');
    } finally {
        await browser.close();
    }
};

const setupPageInterception = async (page: Page) => {
    await page.setRequestInterception(true);
    page.on('request', (req) => {
        const resourceType = req.resourceType();
        if (['image', 'stylesheet', 'font', 'media'].includes(resourceType)) {
            req.abort();
        } else {
            req.continue();
        }
    });
};

const loginToWebsite = async (page: Page, website: Website) => {
    await page.type('input[name="username"]', website.username!, {delay: 100});
    await page.type('input[name="password"]', website.password!, {delay: 100});
    await page.click('button[type="submit"]');
    await page.waitForNavigation({waitUntil: 'domcontentloaded', timeout: 30000});
};

const performSearchAndExtract = async (page: Page, query: string, results: any[], maxPages: number) => {
    try {
        await page.waitForSelector('input[name="keyword"]');
    } catch (error) {
        console.error('Selector not found, taking a screenshot...');
        await page.screenshot({path: 'screenshot.png'});
        throw error;
    }
    await page.type('input[name="keyword"]', query);
    await page.keyboard.press('Enter');
    await page.waitForNavigation({waitUntil: 'domcontentloaded'});

    await extractItemsFromPage(page, results);

    let currentPage = 1;

    while (currentPage < maxPages) {
        const nextPageLink = await page.evaluate(() => {
            const nextPageElement = document.querySelector('.listing-pagination__item--next');
            return nextPageElement ? nextPageElement.getAttribute('data-link') : null;
        });

        if (!nextPageLink) break;

        await page.goto(nextPageLink, {waitUntil: 'domcontentloaded'});

        await extractItemsFromPage(page, results);

        currentPage++;
    }
};

const extractItemsFromPage = async (page: Page, results: any[]) => {
    const items = await page.evaluate(() => {
        return Array.from(document.querySelectorAll('.listing-item')).map(item => {
            const priceString = (item.querySelector('.listing-item__price-new') as HTMLElement)?.innerText || 'N/A';
            const price = parseFloat(priceString.replace(',', '.').replace('€', '').trim()) || Infinity;

            return {
                title: (item.querySelector('.listing-item__name') as HTMLElement)?.innerText || 'N/A',
                price: price,
                link: (item.querySelector('a') as HTMLAnchorElement)?.href || 'N/A',
                availability: (item.querySelector('.listing-item__available') as HTMLElement)?.innerText || 'N/A',
            };
        }).filter(item => item.availability.includes('Auf Lager'));
    });
    results.push(...items);
};

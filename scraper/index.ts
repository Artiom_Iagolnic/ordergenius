import app from './src/app';

const PORT = process.env.PORT || 6000;

app.listen(PORT, () => {
    console.log(`Node.js server is running on port ${PORT}`);
});

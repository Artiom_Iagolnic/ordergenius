"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.scrapeWebsites = void 0;
const puppeteer_extra_1 = __importDefault(require("puppeteer-extra"));
const scrapeWebsites = (query_1, websites_1, ...args_1) => __awaiter(void 0, [query_1, websites_1, ...args_1], void 0, function* (query, websites, maxPages = 2, itemsPerPage = 30) {
    const results = [];
    const browser = yield puppeteer_extra_1.default.launch({
        headless: true,
        args: [
            '--no-sandbox',
            '--disable-setuid-sandbox',
            '--disable-dev-shm-usage',
            '--disable-accelerated-2d-canvas',
            '--disable-gpu',
            '--window-size=1920x1080',
        ],
    });
    try {
        for (const website of websites) {
            const page = yield browser.newPage();
            try {
                yield setupPageInterception(page);
                // Navigate to the website
                yield page.goto(website.url, { waitUntil: 'domcontentloaded' });
                // If login is required, perform login
                if (website.requires_login && website.username && website.password) {
                    yield loginToWebsite(page, website);
                }
                yield performSearchAndExtract(page, query, results, maxPages);
            }
            catch (error) {
                console.error(`Error scraping ${website.url}:`, error);
            }
            finally {
                yield page.close();
            }
        }
        return results.sort((a, b) => a.price - b.price).slice(0, itemsPerPage);
    }
    catch (error) {
        console.error('Error during scraping:', error);
        throw new Error('Scraping failed');
    }
    finally {
        yield browser.close();
    }
});
exports.scrapeWebsites = scrapeWebsites;
const setupPageInterception = (page) => __awaiter(void 0, void 0, void 0, function* () {
    yield page.setRequestInterception(true);
    page.on('request', (req) => {
        const resourceType = req.resourceType();
        if (['image', 'stylesheet', 'font', 'media'].includes(resourceType)) {
            req.abort();
        }
        else {
            req.continue();
        }
    });
});
const loginToWebsite = (page, website) => __awaiter(void 0, void 0, void 0, function* () {
    yield page.type('input[name="username"]', website.username, { delay: 100 });
    yield page.type('input[name="password"]', website.password, { delay: 100 });
    yield page.click('button[type="submit"]');
    yield page.waitForNavigation({ waitUntil: 'domcontentloaded', timeout: 30000 });
});
const performSearchAndExtract = (page, query, results, maxPages) => __awaiter(void 0, void 0, void 0, function* () {
    yield page.waitForSelector('input[name="keyword"]');
    yield page.type('input[name="keyword"]', query);
    yield page.keyboard.press('Enter');
    yield page.waitForNavigation({ waitUntil: 'domcontentloaded' });
    yield extractItemsFromPage(page, results);
    let currentPage = 1;
    while (currentPage < maxPages) {
        const nextPageLink = yield page.evaluate(() => {
            const nextPageElement = document.querySelector('.listing-pagination__item--next');
            return nextPageElement ? nextPageElement.getAttribute('data-link') : null;
        });
        if (!nextPageLink)
            break;
        yield page.goto(nextPageLink, { waitUntil: 'domcontentloaded', timeout: 30000 });
        yield extractItemsFromPage(page, results);
        currentPage++;
    }
});
const extractItemsFromPage = (page, results) => __awaiter(void 0, void 0, void 0, function* () {
    const items = yield page.evaluate(() => {
        return Array.from(document.querySelectorAll('.listing-item')).map(item => {
            var _a, _b, _c, _d;
            const priceString = ((_a = item.querySelector('.listing-item__price-new')) === null || _a === void 0 ? void 0 : _a.innerText) || 'N/A';
            const price = parseFloat(priceString.replace(',', '.').replace('€', '').trim()) || Infinity;
            return {
                title: ((_b = item.querySelector('.listing-item__name')) === null || _b === void 0 ? void 0 : _b.innerText) || 'N/A',
                price: price,
                link: ((_c = item.querySelector('a')) === null || _c === void 0 ? void 0 : _c.href) || 'N/A',
                availability: ((_d = item.querySelector('.listing-item__available')) === null || _d === void 0 ? void 0 : _d.innerText) || 'N/A',
            };
        }).filter(item => item.availability.includes('Auf Lager'));
    });
    results.push(...items);
});

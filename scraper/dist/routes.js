"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const scrapperController_1 = require("./controllers/scrapperController");
const router = (0, express_1.Router)();
router.post('/scrape', scrapperController_1.scrapeController);
exports.default = router;

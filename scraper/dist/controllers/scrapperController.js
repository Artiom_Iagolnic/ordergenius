"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.scrapeController = void 0;
const puppeteerService_1 = require("../services/puppeteerService");
const scrapeController = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { query, websites } = req.body;
    try {
        const results = yield (0, puppeteerService_1.scrapeWebsites)(query, websites);
        res.json(results);
    }
    catch (error) {
        console.error('Error during scraping:', error);
        res.status(500).json({ error: 'Error during scraping' });
    }
});
exports.scrapeController = scrapeController;

<?php

namespace App\Http\Controllers;

use App\Http\Requests\SearchRequest;
use App\Models\Website;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class SearchController extends Controller
{
    public function search(SearchRequest $request): JsonResponse
    {
        $search = $request->input('search');
        $websitesIds = $request->input('websites');

        $websites = Website::whereIn('id', $websitesIds)->get(['url', 'requires_login', 'username', 'password']);

        if ($websites->isEmpty()) {
            return response()->json(['error' => 'No websites found'], 404);
        }
        $scrapedData = $websites->map(function ($website) {
            return [
                'url' => $website->url,
                'requires_login' => $website->requires_login,
                'username' => $website->requires_login ? $website->username : null,
                'password' => $website->requires_login ? $website->password : null
            ];
        });

        Log::info('Scraped data', $scrapedData->toArray());
        $response = Http::timeout(60)->post('http://localhost:6000/scrape', [
            'query' => $search,
            'websites' => $scrapedData->toArray(),
        ]);

        return response()->json($response->json());
    }
}

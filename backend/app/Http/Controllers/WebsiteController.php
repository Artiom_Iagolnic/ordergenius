<?php

namespace App\Http\Controllers;

use App\Http\Requests\WebsiteRequest;
use App\Models\Website;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class WebsiteController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(): JsonResponse
    {
        try {
            return response()->json([
                'websites' => Website::all(),
                'message' => 'Websites fetched successfully',
            ], 200
            );
        } catch (\Exception $e) {
            return response()->json([
                'message' => 'An error occurred while fetching websites',
                'error' => $e->getMessage()
            ], 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(WebsiteRequest $request): JsonResponse
    {
        Log::info('Request data: ', $request->all());
        try {
            return response()->json([
                'website' => Website::create($request->all()),
                'message' => 'Website created successfully'
            ], 201);
        } catch (\Exception $e) {
            return response()->json([
                'message' => 'An error occurred while creating website',
                'error' => $e->getMessage()
            ], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(WebsiteRequest $request, string $id): JsonResponse
    {
        try {
            $website = Website::find($id);
            $website->update($request->all());
            return response()->json([
                'website' => $website,
                'message' => 'Website updated successfully'
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'message' => 'An error occurred while updating website',
                'error' => $e->getMessage()
            ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id): JsonResponse
    {
        try {
            Website::destroy($id);
            return response()->json([
                'message' => 'Website deleted successfully'
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'message' => 'An error occurred while deleting website',
                'error' => $e->getMessage()
            ], 500);
        }
    }
}

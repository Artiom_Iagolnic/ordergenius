<?php

use App\Http\Controllers\SearchController;
use App\Http\Controllers\WebsiteController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::middleware(['auth:sanctum'])->get('/user', function (Request $request) {
    return $request->user();
});
Route::middleware(['auth:sanctum'])->post('/search', [SearchController::class, 'search']);

Route::middleware(['auth:sanctum'])->apiResource('/websites', WebsiteController::class);
